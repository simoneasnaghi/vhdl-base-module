# VHDL base module

Basic architecture in VHDL language
List:
- [AND gate](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/AND_GATE.vhd)
- [NAND gate](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/NAND_GATE.vhd)
- [OR gate](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/OR_GATE.vhd)
- [NOR gate](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/NOR_GATE.vhd)
- [XOR gate](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/XOR_GATE.vhd)
- [XNOR gate](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/XNOR_GATE.vhd)
- [J-K latch](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/JK_LATCH.vhd)
- [D latch](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/dlatch.vhd)
- [SR latch](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/SR_LATCH.vhd)
- [D flip flop](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/d_ff.vhd)
- [Asynchronous D flip flop](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/FLIPFLOP_D_ASINCRONO.vhd)
- [MUX 2:1](https://gitlab.com/simoneasnaghi/vhdl-base-module/-/blob/main/MUX2_1.vhd)
